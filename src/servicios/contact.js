const baseUrl = 'https://api.lmrecicladora.com';

const newContact = (data, callback, callbackError) => {

    fetch(`${baseUrl}/api/get-token`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .then(res => {
        if(res.status === 200) {
            data.authorization = res.token;

            fetch(`${baseUrl}/api/new-contact`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': 'kBVSgqsAdah6KDRaLB4UFdDbb3kPcbBD2z27d5Oj'
                },
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                if(res.status === 200) {
                    callback();
                }else {
                    callbackError()
                }
            })
        }
    })
}

export default newContact;
