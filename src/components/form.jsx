import React, { useState } from "react";
import styled from "styled-components";
import Input, { TextBox } from "./../widgets/input";
import Button from "./../widgets/button";
import Svg from "./../widgets/svg";

import image from "./../assets/contact.png";
import phoneNumber from "./../assets/svg/phone.svg";
import mail from "./../assets/svg/mail.svg";
import facebook from "./../assets/facebook.svg";
import whatsapp from "./../assets/whatsapp2.svg";
import mapa from "./../assets/mapa.jpg";

import newContact from "./../servicios/contact";
import { WhatsApp, MailContact, WhatsAppFormated } from "./../constants";

const Section = styled.div`
    min-height: 70vh;
    width: 100%;
    padding: 2rem;
    background-color: #fff;
`;

const Formulario = styled.div`
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 0 5px 25px -2px rgba(0, 0, 0, 0.2);
    overflow: hidden;
`;

const Form = () => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    // const [company, setCompany] = useState("");
    const [message, setMessage] = useState("");

    const [loading, setLoading] = useState(false);
    const [loading2, setLoading2] = useState(false);

    const [data, setData] = useState({
        name: "",
        email: "",
        phone: "",
        // company: "",
        message: "",
    });

    const saveData = (provider) => {
        provider ? setLoading2(true) : setLoading(true);
        setData({
            name: name,
            email: email,
            phone: phone,
            // company: company,
            message: message,
        });

        let msg = "";

        if (provider) {
            msg = `
                Hola, mi nombre es %2A${name}%2A, estoy interesado en formar parte de su equipo de trabajo en %2ALM Recicladora S.A de C.V%2A, me gustaria programar una entrevista con ustedes, les dejo mis datos de contacto:%0D%0A
                %0D%0A
                - ${phone}%0D%0A
                - ${email}%0D%0A
                %0D%0A
                ${message}.%0D%0A
                %0D%0A
                Sin más por el momento, agradezco su atención.
            `;
        } else {
            msg = `
                Hola, mi nombre es %2A${name}%2A, he llenado el formulario de su página web lmrecicladora.com me gustaria m[as información, le dejo mis datos de contacto:%0D%0A
                %0D%0A
                - ${phone}%0D%0A
                - ${email}%0D%0A
                %0D%0A
                ${message}.%0D%0A
                %0D%0A
                Sin más por el momento, agradezco su atención.
            `;
        }

        window.open(
            `https://wa.me/${WhatsApp}/?text=${msg}`,
            "Contacto con LM Recicladora",
            "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=400, height=400"
        );

        setTimeout(() => {
            setLoading(false);
            setLoading2(false);
        }, 3000);
    };

    return (
        <Section>
            <div className="flex:wrapper">
                <div className="flex flex:center">
                    <div className="m:width-20 d:width-18">
                        <h3 className="width-20 size:large-m size:hero-d mb:3 t:block t:center">
                            Ubícanos
                        </h3>

                        <div className="flex flex:center mb:4">
                            <a
                                className="width-20"
                                href="https://goo.gl/maps/8WmF6ba6JR9bJkgGA"
                                target="_blank"
                            >
                                <img
                                    src={mapa}
                                    alt="ubicacion"
                                    style={{
                                        width: "100%",
                                        height: "500px",
                                        borderRadius: "20px",
                                        overflow: "hidden",
                                        objectFit: "cover",
                                    }}
                                />
                            </a>

                            <p className="t:block t:center width-14 size:medium">
                                Carretera panamericana km 23, 11379 Col .
                                Valledorado Cd Juárez Chihuahua. Cp 32573
                                <br />
                                Whatsapp: {WhatsAppFormated}
                            </p>
                        </div>
                    </div>
                    <div className="m:width-20 d:width-18">
                        <h3 className="width-20 size:large-m size:hero-d mb:3">
                            Contacto
                        </h3>
                    </div>
                    <Formulario className="flex m:width-20 d:width-18">
                        <div className="m:width-20 d:width-10 p:2">
                            <p className="size:large">¡Mándanos un mensaje!</p>
                            <p className="mt:1:5">
                                Si estas interesado en alguno de nuestros
                                servicios o tienes alguna duda no dudes en
                                mandarnos un mensaje.
                            </p>

                            <div className="flex width-20 items:start">
                                <div className="width-20 p:0:5">
                                    <Input
                                        placeholder="Nombre"
                                        value={name}
                                        getValue={(v) => setName(v)}
                                    />
                                </div>
                                <div className="width-20 p:0:5">
                                    <Input
                                        placeholder="Email"
                                        value={email}
                                        getValue={(v) => setEmail(v)}
                                    />
                                </div>
                                <div className="width-20 p:0:5">
                                    <Input
                                        placeholder="Telefono"
                                        value={phone}
                                        getValue={(v) => setPhone(v)}
                                    />
                                </div>
                                {/* <div className="m:width-20 d:width-10 p:0:5">
                                    <Input
                                        placeholder="Compañia"
                                        value={company}
                                        getValue={(v) => setCompany(v)}
                                    />
                                </div> */}
                                <div className="width-20 p:0:5">
                                    <TextBox
                                        placeholder="Mensaje"
                                        value={message}
                                        getValue={(v) => setMessage(v)}
                                    />
                                </div>
                            </div>

                            <div className="flex">
                                <div className="flex m:width-20 d:width-20 items:center pr:1">
                                    <Svg icon={phoneNumber} w={18} h={18} />
                                    <p className="pl:0:5">{WhatsAppFormated}</p>
                                </div>

                                <div className="flex m:width-20 d:width-20 items:center pr:1">
                                    <Svg icon={mail} w={18} h={18} />
                                    <p className="pl:0:5">{MailContact}</p>
                                </div>
                            </div>

                            <div className="flex flex:center width-20 justify:start-d pv:1">
                                <div className="p:0:5">
                                    <Button
                                        loader={loading}
                                        primary
                                        Click={() => saveData(false)}
                                    >
                                        ¡Contactar ahora!
                                    </Button>
                                </div>
                                <div className="p:0:5">
                                    <Button
                                        loader={loading2}
                                        secondary
                                        Click={() => saveData(true)}
                                    >
                                        ¡Unirme al equipo!
                                    </Button>
                                </div>
                            </div>
                        </div>
                        <div
                            className="m:width-20 d:width-10"
                            style={{
                                background: `url(${image}) no-repeat center`,
                                backgroundSize: "cover",
                                transform: "translateX(20px)",
                            }}
                        ></div>
                    </Formulario>
                </div>
            </div>

            <div className="flex flex:center pv:4 ph:1">
                <a
                    href="https://www.facebook.com/lmrecicladora"
                    target="_blank"
                    className="ph:1"
                >
                    <img
                        src={facebook}
                        alt="Facebook"
                        width="50"
                        height="50"
                        style={{
                            objectFit: "contain",
                        }}
                    />
                </a>
                <a
                    href={`https://wa.me/${WhatsApp}/?text=Quiero%20más%20información`}
                    target="_blank"
                    className="ph:1"
                >
                    <img
                        src={whatsapp}
                        alt="WhatsApp"
                        width="50"
                        height="50"
                        style={{
                            objectFit: "contain",
                        }}
                    />
                </a>
            </div>
        </Section>
    );
};

export default Form;
