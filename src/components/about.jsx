import React from "react";
import styled from "styled-components";

import logo from "./../assets/logo.png";
import background from "./../assets/about.jpg";

import useWidth from "../hooks/useWidth";

const Section = styled.div`
    min-height: 70vh;
    color: black;
`;

const Text = styled.p`
    display: block;
    text-align: justify;
`;

const About = () => {
    const { isMobile } = useWidth();
    return (
        <Section className="flex width-20">
            <div
                className="flex justify:center m:width-20 d:width-10"
                style={{
                    backgroundColor: "white",
                }}
            >
                <div className="width-15 pv:3">
                    <div className="flex width-20 items:center justify:between mb:2">
                        <h3 className="size:large-m size:title-d">Nosotros</h3>
                    </div>

                    <div className="size:medium">
                        <Text>
                            Somos una empresa de compra y venta de materiales
                            ferrosos en altos volúmenes. Estamos comprometidos a
                            brindar el mejor servicio y precios competitivos a
                            nuestros clientes para que puedan recuperar el costo
                            de su inversión.
                        </Text>

                        <div className="mv:1:5">
                            <strong>VISIÓN</strong>
                            <Text className="mt:1">
                                Consolidarnos como una de las mejores empresas a
                                nivel binacional mediante trabajo honesto, un
                                excelente trato a nuestros clientes y siendo
                                altamente competitivos con nuestros precios y la
                                amplia gama de servicios que ofrecemos.
                            </Text>
                        </div>
                        <div className="mv:1:5">
                            <strong>MISIÓN</strong>
                            <Text className="mt:1">
                                Nuestra misión es ser una empresa social y
                                ecológicamente responsable que busca fomentar en
                                la sociedad una cultura de conciencia y respeto
                                al Medio Ambiente mediante el reciclaje, dando
                                un servicio eficiente, de calidad y honradez.
                            </Text>
                        </div>
                        <div className="mv:1:5">
                            <strong>VALORES</strong>
                            <Text className="mt:1">
                                Innovación, confianza, integridad, honestidad,
                                transparencia y responsabilidad social.
                            </Text>
                        </div>
                    </div>
                </div>
            </div>
            {!isMobile && (
                <div
                    className="flex flex:center m:width-20 d:width-10"
                    style={{
                        background: `url(${background}) no-repeat center`,
                        backgroundSize: "cover",
                        backgroundAttachment: "fixed",
                    }}
                >
                    <img
                        src={logo}
                        alt="logotipo"
                        width="200"
                        height="200"
                        style={{
                            objectFit: "contain",
                        }}
                    />
                </div>
            )}
        </Section>
    );
};

export default About;
