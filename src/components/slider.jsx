import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";

import slide1 from "./../assets/slide/1.jpg";
import slide2 from "./../assets/slide/2.jpg";
import slide3 from "./../assets/slide/3.jpg";
import slide4 from "./../assets/slide/4.jpg";
import slide5 from "./../assets/slide/5.jpg";
import slide6 from "./../assets/slide/6.jpg";
import slide7 from "./../assets/slide/7.jpg";
import slide8 from "./../assets/slide/8.jpg";
import slide9 from "./../assets/slide/9.jpg";
import slide10 from "./../assets/slide/10.jpg";

const SliderWrapper = styled.div`
    overflow: hidden;
    width: 100%;
    padding: 100px 2rem 1rem 2rem;
    position: relative;
`;

const SliderContainer = styled.div`
    display: flex;
    min-width: ${({ count }) => count || 1}00%;
    flex-wrap: nowrap;
    position: relative;
    transition: margin-left 0.2s ease-in;
`;

const SliderItem = styled.div`
    display: block;
    width: 100%;
    max-width: 800px;
    height: 400px;

    img {
        width: 100%;
        height: 100%;
        object-fit: cover;
        padding: 1rem;
    }
`;

const Dot = styled.button`
    display: block;
    width: 15px;
    min-width: 15px;
    min-height: 15px;
    height: 15px;
    border-radius: 2rem;
    background-color: #ddd;
    cursor: pointer;
    transition: all 0.2s ease;

    &:hover {
        background-color: #bbb;
    }

    &:active,
    &:focus {
        background-color: #000000;
    }

    &:not(:last-child) {
        margin-right: 1rem;
    }
`;

const sliders = [
    slide1,
    slide2,
    slide3,
    slide4,
    slide5,
    slide6,
    slide7,
    slide8,
    slide9,
    slide10,
];

const Slider = () => {
    const slider = useRef(null);
    const [slide, setSlide] = useState(0);

    useEffect(() => {
        slider.current.style.marginLeft = `-${slide}px`;
    }, [slide]);

    const handleSlide = (index) => {
        setSlide(index * 600);
    };

    return (
        <div
            className="width-20"
            style={{
                backgroundColor: "white",
            }}
        >
            <SliderWrapper>
                <SliderContainer ref={slider} count={10}>
                    {sliders.map((item, index) => (
                        <SliderItem key={index}>
                            <img src={item} alt="imagen de servicio" />
                        </SliderItem>
                    ))}
                </SliderContainer>
            </SliderWrapper>
            <div className="width-20 flex flex:center p:1">
                {sliders.map((item, index) => (
                    <Dot key={index} onClick={() => handleSlide(index)} />
                ))}
            </div>
        </div>
    );
};

export default Slider;
