import React from "react";
import styled from "styled-components";
import Button from "./../widgets/button";

import { WhatsApp } from "./../constants";

import trailer from "../assets/trailer.jpg";
import personas from "../assets/personas.jpg";
import compra from "../assets/compraventa.jpg";
import subcontratacion from "../assets/subcontratacion.jpg";
import bascula from "../assets/bascula.jpg";

const Section = styled.div`
    min-height: 70vh;
    color: black;
    
    & div {
      z-index: 2;
    }
`;

const Circle = styled.div`
    background: rgb(37, 61, 186);
    background: linear-gradient(
        90deg,
        rgba(37, 61, 186, 1) 0%,
        rgba(19, 20, 77, 1) 100%
    );
    width: 130vw;
    height: 130vw;
    min-width: 130vw;
    min-height: 130vw;
    border-radius: 100%;
    position: absolute;
    right: -100vw;
    z-index: 1;
    pointer-events: none;
`;

const servicios = [
    "Asesoría",
    "Almacenamiento",
    "Compra y Venta de Chatarra (Báscula para 80 toneladas)",
    "Exportación",
    "Importación",
    "Logística",
];

const servicios2 = [
    "Renta de maquinaria",
    "Rampa para descarga (Grúas)",
    "Retroexcavadoras",
    "Dompes",
];

const Servicios = () => {
    const navigate = (url) => {
        if (typeof window !== "undefined") {
            window.location.href = url;
        }
    };

    return (
        <>
            <Section
                className="flex items:start justify:center width-20 mv:4"
                style={{ position: "relative" }}
            >
                <Circle />
                <h3 className="width-20 size:large-m size:hero-d t:center t:block marginless">
                    Servicios
                </h3>
                <strong className="width-20 size:large-d t:center t:block mt:0 mb:3">
                    Descubre cada uno de los servicios que ofrecemos
                </strong>
                <div className="flex m:width-20 d:width-15">
                    <div className="m:width-20 d:width-10 p:1">
                        <ul>
                            {servicios.map((serv, index) => (
                                <li key={index} className="size:medium">
                                    • {serv}
                                </li>
                            ))}
                        </ul>
                    </div>
                    <div className="m:width-20 d:width-10 p:1">
                        <ul>
                            {servicios2.map((serv, index) => (
                                <li key={index} className="size:medium">
                                    • {serv}
                                </li>
                            ))}
                        </ul>
                    </div>

                    <div className="flex flex:center width-20 pv:1">
                        <Button
                            primary
                            className="ph:3"
                            onClick={() =>
                                navigate(
                                    `https://wa.me/${WhatsApp}/?text=Quiero%20más%20información`
                                )
                            }
                        >
                            ¡Cotiza con nosotros ahora!
                        </Button>
                    </div>
                </div>
            </Section>
            <Section style={{ backgroundColor: 'white' }} className="ph:2">
                <div className="flex:wrapper">
                    <div className="flex items:start pt:4">
                        <div className="flex m:width-20 tb:width-15 items:center">
                            <div className="flex flex:center m:width-20 tb:width-5">
                                <img
                                    src={trailer}
                                    width={160}
                                    height={160}
                                    style={{ objectFit: "contain" }}
                                />
                            </div>
                            <div className="flex flex:center m:width-20 tb:width-15">
                                <strong className="size:large width-20 mb:1">
                                    Transporte
                                </strong>
                                <ul className="width-20 size:medium">
                                    <li>
                                        • Sistema de recolección directamente a
                                        tu establecimiento.
                                    </li>
                                    <li>
                                        • Movimientos locales y de cruce
                                        fronterizo.
                                    </li>
                                    <li>
                                        • Almacenaje y recepción de materiales,
                                        maquinaria o transporte.
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="flex m:width-20 tb:width-15 items:center mt:2">
                            <div className="flex flex:center m:width-20 tb:width-5">
                                <img
                                    src={personas}
                                    width={160}
                                    height={160}
                                    style={{ objectFit: "contain" }}
                                />
                            </div>
                            <div className="flex flex:center m:width-20 tb:width-15">
                                <strong className="size:large width-20 mb:1">
                                    Personal
                                </strong>
                                <ul className="width-20 size:medium">
                                    <li>
                                        Contamos con personal altamente
                                        capacitado para brindarte la mejor
                                        atención y servicio en menor tiempo.
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="flex m:width-20 tb:width-15 items:center mt:2">
                            <div className="flex flex:center m:width-20 tb:width-5">
                                <img
                                    src={compra}
                                    width={160}
                                    height={160}
                                    style={{ objectFit: "contain" }}
                                />
                            </div>
                            <div className="flex flex:center m:width-20 tb:width-15">
                                <strong className="size:large width-20 mb:1">
                                    Compra y Venta
                                </strong>
                                <ul className="width-20 size:medium">
                                    <li>• Metales y Scrap.</li>
                                    <li>• Transportación.</li>
                                    <li>• Importación de materiales.</li>
                                    <li>• Exportación de materiales.</li>
                                </ul>
                            </div>
                        </div>
                        <div className="flex m:width-20 tb:width-15 items:center mt:2">
                            <div className="flex flex:center m:width-20 tb:width-5">
                                <img
                                    src={subcontratacion}
                                    width={160}
                                    height={160}
                                    style={{ objectFit: "contain" }}
                                />
                            </div>
                            <div className="flex flex:center m:width-20 tb:width-15">
                                <strong className="size:large width-20 mb:1">
                                    Subcontratación
                                </strong>
                                <ul className="width-20 size:medium">
                                    <li>
                                        Ofrecemos una solución a tus necesidades
                                        inmediatas en transporte. Ahorra tiempo,
                                        costos y esfuerzo.
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="flex m:width-20 tb:width-15 items:center mt:2">
                            <div className="flex flex:center m:width-20 tb:width-5">
                                <img
                                    src={bascula}
                                    width={160}
                                    height={160}
                                    style={{ objectFit: "contain" }}
                                />
                            </div>
                            <div className="flex flex:center m:width-20 tb:width-15">
                                <strong className="size:large width-20 mb:1">
                                    Báscula Electrónica
                                </strong>
                                <ul className="width-20 size:medium">
                                    <li>
                                        Pesaje exacto de tus materiales.
                                        ¡Pagamos tu peso en pesos!
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </Section>
            <Section style={{ backgroundColor: 'white' }} className="ph:2">
                <div className="flex:wrapper">
                    <div className="pv:4">
                        <h3 className="width-20 size:large size:hero-d mb:3">
                            Productos
                        </h3>

                        <strong className="size:large width-20 mb:1">
                            Material Ferroso y No Ferroso
                        </strong>
                        <p className="width-20 size:medium mb:2">
                            Fierro, aluminio, cobre, bronce, acero inoxidable.
                        </p>
                        <strong className="size:large width-20 mb:1">
                            Sector Automotriz
                        </strong>
                        <p className="width-20 size:medium mb:2">
                            Transmisiones, motores, chasis, radiadores,
                            acumuladores y carrocerias.
                        </p>
                        <strong className="size:large width-20 mb:1">
                            Construcción
                        </strong>
                        <p className="width-20 size:medium mb:2">
                            Varilla, estructuras metálicas, vigas, escaleras y
                            andamios.
                        </p>
                    </div>
                </div>
            </Section>
        </>
    );
};

export default Servicios;
