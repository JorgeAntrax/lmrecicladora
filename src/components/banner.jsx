import React from "react";
import styled from "styled-components";
import Svg from "./../widgets/svg";
import Button from "./../widgets/button";
import Animate from "./../widgets/animate";
import icon2 from "./../assets/svg/lp2.svg";
import icon from "./../assets/svg/lp1.svg";
import { WhatsApp } from "./../constants";

import slide5 from "./../assets/slide/5.jpg";

import useWidth from "../hooks/useWidth";

const BanneWrapper = styled.div`
    min-height: 100vh;
    height: 100%;
    color: black;
`;

const Relative = styled.div`
    position: relative;
    width: 100%;
    height: 100%;
`;

const Banner = () => {
    const { isMobile } = useWidth();
    const navigate = (url) => {
        if (typeof window !== "undefined") {
            window.location.href = url;
        }
    };

    return (
        <div className="flex justify:center items:start">
            <BanneWrapper className="flex:wrapper">
                <div
                    className="flex width-20 items:center justify:center"
                    style={{ minHeight: "70vh" }}
                >
                    <div
                        className="flex items:center justify:center m:width-20 tb:width-6"
                        style={{
                            minHeight: "400px",
                        }}
                    >
                        <Relative className="flex items:center justify:center">
                            <Svg
                                icon={icon}
                                w={isMobile ? 50 : 100}
                                h={isMobile ? 50 : 100}
                            />
                            <Animate>
                                <Svg
                                    icon={icon2}
                                    w={isMobile ? 150 : 300}
                                    h={isMobile ? 150 : 300}
                                />
                            </Animate>
                        </Relative>
                    </div>
                    <div className="m:width-20 tb:width-14 pv:2 ph:3">
                        <h1 className="size:title-m size:hero-d t:center t:left-d">
                            Recicladora y Logística S.A de C.V
                        </h1>
                        <p className="mt:1 t:center t:left-d size:title-d">
                            Ciudad Juárez, Chihuahua, MX.
                        </p>
                    </div>
                </div>
            </BanneWrapper>
        </div>
    );
};

export default Banner;
