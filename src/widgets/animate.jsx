import React from 'react'
import styled, { keyframes } from 'styled-components'

const rotate = keyframes`

    from {
        transform: translate(-50%,-50%) rotate(0deg);
    }
    to {
        transform: translate(-50%,-50%) rotate(360deg);
    }

`

const Animation = styled.div`
    animation: ${rotate} 5s linear infinite;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%) rotate(0deg);
`

const Animate = ({children, ...props}) => (
    <Animation>{children}</Animation>
)

export default Animate;
