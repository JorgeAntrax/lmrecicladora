import React from "react";
import styled, { css, keyframes } from "styled-components";

const rotateLoader = keyframes`

    from {
        transform: translate(-50%,-50%) rotate(0deg);
    }

    to {
        transform: translate(-50%,-50%) rotate(360deg);
    }

`;

const ButtonWrapper = styled.button`
    position: relative;
    display: inline-flex;
    text-align: center;
    align-items: center;
    align-content: center;
    justify-content: center;
    line-height: 1.4;
    font-size: inherit;
    border-radius: 5px;
    padding: 8px 24px;
    overflow: hidden;
    cursor: pointer;
    transition: all 0.2s linear;

    &,
    &:hover,
    &:focus,
    &:active {
        outline: 0;
    }

    .spin {
        width: ${({ spinSize }) => spinSize || 20}px;
        min-width: ${({ spinSize }) => spinSize || 20}px;
        height: ${({ spinSize }) => spinSize || 20}px;
        min-height: ${({ spinSize }) => spinSize || 20}px;

        border-radius: 2rem;
        border: 2px solid transparent;
        border-top-color: rgba(255, 255, 255, 0.7);
        border-right-color: rgba(255, 255, 255, 0.5);
        border-bottom-color: rgba(255, 255, 255, 0.3);
        border-left-color: rgba(255, 255, 255, 0.1);

        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) rotate(0deg);
        animation: ${rotateLoader} 1s linear infinite;
    }

    * {
        pointer-events: none;
    }

    .content {
        opacity: ${({ loader }) => (loader ? 0 : 1)};
        user-select: none;
    }

    ${({ primary }) =>
        primary
            ? css`
                  background-color: #e22a3f;
                  color: #ffffff;

                  &:hover {
                      background-color: #bf2a3f;
                  }
              `
            : ""}
    ${({ secondary }) =>
        secondary
            ? css`
                  background-color: #f6f6f6;
                  color: #0a0a0a;

                  &:hover {
                      background-color: #233432;
                      color: white;
                  }
              `
            : ""}
`;

const Button = ({ Click, children, loader, ...props }) => (
    <ButtonWrapper onClick={() => Click()} loader={loader} {...props}>
        {loader && <span className="spin"></span>}
        <span className="content">{children}</span>
    </ButtonWrapper>
);

export default Button;
