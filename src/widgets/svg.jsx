import React from 'react'
import styled from 'styled-components'

const SvgWrapper = styled.div`
    display: block;
    width: ${ ({w}) => w}px;
    height: ${ ({h}) => h}px;

    img {
        width: 100%;
        height: 100%;
        object-fit: contain;
    }
`

const Svg = ({ icon, w, h}) => {
    return (
        <SvgWrapper w={w} h={h}>
            <img src={icon} alt="svg" />
        </SvgWrapper>
    )
}

export default Svg
