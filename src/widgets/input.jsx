import React from "react";
import styled from "styled-components";

const InputWrapper = styled.input`
    appearance: none;
    border-bottom: 1px solid black;
    color: #444;
    display: flex;
    width: 100%;
    padding: 8px 0;
    font-size: inherit;
`;

const TextWrapper = styled.textarea`
    appearance: none;
    border-bottom: 1px solid black;
    color: #444;
    display: flex;
    width: 100%;
    min-height: 80px;
    padding: 8px 0;
    resize: vertical;
    font-size: inherit;
`;

const Input = ({ getValue, id, placeholder, value }) => {
    const handleChange = (e) => {
        getValue && getValue(e.target.value);
    };

    return (
        <InputWrapper
            id={id}
            value={value}
            placeholder={placeholder}
            onChange={(e) => handleChange(e)}
        />
    );
};

const TextBox = ({ getValue, id, placeholder, value }) => {
    const handleChange = (e) => {
        getValue && getValue(e.target.value);
    };

    return (
        <TextWrapper
            id={id}
            value={value}
            placeholder={placeholder}
            onChange={(e) => handleChange(e)}
        />
    );
};

export { TextBox };
export default Input;
