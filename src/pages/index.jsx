import React from "react";
import Banner from "./../components/banner";
import About from "./../components/about";
import Servicios from "./../components/servicios";
import Slider from "./../components/slider";
import Contacto from "./../components/form";

const IndexPage = () => (
    <>
        <Banner />
        <About />
        <Servicios />
        <Slider />
        <Contacto />
    </>
);

export default IndexPage;
