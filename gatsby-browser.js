import 'brand-digitalbooting';
import React from "react";
import Layout from "./src/modules/layout";
import "./global.css";
import "./index.css";

export const wrapRootElement = ({ element }) => <Layout>{element}</Layout>;
