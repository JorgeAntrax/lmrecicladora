module.exports = {
    pathPrefix: "",
    siteMetadata: {
        title: "Recicladora y Logística en CD Juárez - LM Recicladora",
        description:
            "Asesoría, Logística, Almacenamiento, Importación, Exportación,Renta de maquinaria, Reciclaje, Compra y venta de chatarra, Báscula para 80 toneladas, Rampa para descarga, Grúas, Retroexcavadoras, Dompes",
        url: "https://lmrecicladora.com",
        author: "digitalbooting.com",
        image: "https://lmrecicladora.com/poster.jpg",
        seokeywords: [
            "recicladora",
            "recicladora y logística",
            "recicladora y logística en cd juarez",
        ],
    },
    plugins: [
        `gatsby-plugin-styled-components`,
        `gatsby-plugin-react-helmet`,
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "images",
                path: `${__dirname}/src/assets`,
            },
        },
    ],
};
